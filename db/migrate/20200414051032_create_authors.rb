class CreateAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :image
      t.belongs_to :content

      t.timestamps
    end
  end
end
