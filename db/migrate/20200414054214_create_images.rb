class CreateImages < ActiveRecord::Migration[6.0]
  def change
    create_table :images do |t|
      t.string :image
      t.belongs_to :content
      
      t.timestamps
    end
  end
end
