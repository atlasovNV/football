class Content < ApplicationRecord
    has_many :authors
    has_many :images


    accepts_nested_attributes_for :images, 
                                  :authors

end
