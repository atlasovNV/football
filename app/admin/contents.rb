ActiveAdmin.register Content do
    permit_params :title,
                  images_attributes: [:id, :image],
                  authors_attributes: [:id, :name, :image]
  
    form name: 'Контент' do |f|
      inputs 'Название статьи' do
            input :title
            input :body, as: :quill_editor
        end
            inputs 'Картинка для статьи' do
                f.inputs do
                f.has_many :images, new_record: true, allow_destroy: true do |r|
                    r.input :image
                end
            end
        end

        inputs 'Автор статьи' do
            f.inputs do
                f.has_many :authors, new_record: true, allow_destroy: true do |r|
                r.input :name
                r.input :image
                end
            end
        end
        actions
    end
end





